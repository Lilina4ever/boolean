/* Beregner egenskaber ud fra boolsk algebra WOOP WOOP*/
#include <iostream>
#include <string>
#include <vector>
using namespace std;

// Array til mænd - vektorer bruges i stedet for array så størrelsen nemt kan findes.
vector<string> M = {"Jens", "Viktor", "Birk", "Markus"};
// Array til børn
vector<string> C = {"Viktor", "Birk", "Markus", "Ida"};
// Array til venstrehåndede
vector<string> L = {"Birk", "Markus", "Dorte", "Marie","Jens"};

/* Undersøger om array indeholder element. */
bool contains(vector<string> liste, string element) {
  for (int i = 0; i < liste.size(); i++) {
    // Returner sand, hvis dette er elementet.
    if (liste[i] == element){
            return true;
    }
  }
  // Returner falsk, hvis den ikke blev fundet.
  return false;
}

/* Undersøger om name er hunkøn. */
bool isFemale(string name) {
    return !contains(M, name);
}

/* Undersøger om name er en dreng. */
bool isBoy(string name) {
  return contains(M, name) && contains(C, name);
}

/* Undersøger om name er kvinde eller barn */
bool isWomanOrChild(string name) {
  return !contains(M, name) || contains(C, name);
}

/* Unders�ger om name er en h�jreh�ndet voksen mand*/
bool isRightHandedMan(string name) {
  return contains(M, name) && !contains(L, name);
}

/* Unders�ger om name er en venstreh�ndet kvinde eller barn*/
bool isLeftHandedWomanOrChild(string name) {

  return isWomanOrChild(name) && contains(L, name);
}


int main(){

    vector<string> test = {"Jens","Dorte","Markus","Eva","Birk"};

    for(int i = 0; i < test.size(); i++){
        cout << "Er " << test[i] << " en kvinde?" << endl;
        if(isFemale(test[i])){
            cout << "Sandt" << endl;
        } else {
            cout << "Falsk" << endl;
        }
    }

    for(int i = 0; i < test.size(); i++){
        cout << "Er " << test[i] << " en dreng?" << endl;
        if(isBoy(test[i])){
            cout << "Sandt" << endl;
        } else {
            cout << "Falsk" << endl;
        }
    }

    for(int i = 0; i < test.size(); i++){
        cout << "Er " << test[i] << " en kvinde eller et barn?" << endl;
        if(isWomanOrChild(test[i])){
            cout << "Sandt" << endl;
        } else {
            cout << "Falsk" << endl;
        }
    }

        for(int i = 0; i < test.size(); i++){
        cout << "Er " << test[i] << " en hoejrehaandet mand?" << endl;
        if(isRightHandedMan(test[i])){
            cout << "Sandt" << endl;
        } else {
            cout << "Falsk" << endl;
        }
    }

            for(int i = 0; i < test.size(); i++){
        cout << "Er " << test[i] << " en venstrehaandet kvinde eller barn?" << endl;
        if(isLeftHandedWomanOrChild(test[i])){
            cout << "Sandt" << endl;
        } else {
            cout << "Falsk" << endl;
        }
    }

    return 0;
}
